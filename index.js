const dotenv = require('dotenv').config();
const express = require('express');
const app = express();
const router = express.Router();
const crypto = require('crypto');
const cookie = require('cookie');
const nonce = require('nonce')();
const querystring = require('querystring');
const request = require('request-promise');
const shopifyAPI = require('shopify-node-api');
const Shopify = require('shopify-api-node');

const apiKey = process.env.SHOPIFY_API_KEY;
const apiSecret = process.env.SHOPIFY_API_SECRET;
const scopes = 'read_products, write_products';
const forwardingAddress = "http://d9baf80e.ngrok.io";
const shopName = 'yumi-commerce.myshopify.com';

const redirectUri = forwardingAddress + '/shopify/callback';

const fs = require('fs');
const csv = require('csvtojson');
const Json2csvParser = require('json2csv').Parser;

const shopify = new Shopify({
    shopName: shopName,
    apiKey: apiKey,
    password: apiSecret
  });

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.listen(8080, () => {
  console.log('Example app listening on port 8080!');
});

app.get('/shopify', (req, res) => {
    var Shopify = new shopifyAPI({
        shop: shopName, 
        shopify_api_key: apiKey, 
        shopify_shared_secret: apiSecret, 
        shopify_scope: scopes,
        redirect_uri: redirectUri,
        nonce: '' // you must provide a randomly selected value unique for each authorization request
    });

    // Building the authentication url
    var auth_url = Shopify.buildAuthURL();
 
    // Assuming you are using the express framework
    // you can redirect the user automatically like so
    res.redirect(auth_url);
});

app.get('/shopify/callback', (req, res) => {
    var Shopify = new shopifyAPI({
        shop: shopName, 
        shopify_api_key: apiKey, 
        shopify_shared_secret: apiSecret, 
        shopify_scope: scopes,
        redirect_uri: redirectUri,
        nonce: '' // you must provide a randomly selected value unique for each authorization request
    });
    query_params = req.query;
 
    Shopify.exchange_temporary_token(query_params, function(err, data){
        // This will return successful if the request was authentic from Shopify
        // Otherwise err will be non-null.
        // The module will automatically update your config with the new access token
        // It is also available here as data['access_token']
    });
});

app.get('/export-smart-collections', (req, res) => {
    const shopify = new Shopify({
        shopName: shopName,
        accessToken: process.env.access_token
    });

    shopify.smartCollection.list()
    .then(collections => {
        const fields = ['title', 'sort_order'];
        try {
            const parser = new Json2csvParser(fields);
            const csv = parser.parse(collections);
            fs.writeFile('smartCollection.csv', csv, 'utf8', function(err) {
                if (err) {
                    console.log('Error parsing json to csv');
                } else {
                    console.log('File saved');
                }
            });
        } catch (err) {
            console.error(err);
        }
    })
    .catch(err => console.error(err));
});

app.get('/import-smart-collections', (req, res) => {
    const shopify = new Shopify({
        shopName: shopName,
        accessToken: process.env.access_token
    });
    const csvFilePath = 'import.csv';
    csv()
    .fromFile(csvFilePath)
    .then((collectionObject) => {
        for (i in collectionObject) {
            shopify.smartCollection.create(collectionObject[i])
            .then(
                smartCollection => console.log(smartCollection),
                err => console.error(err)
            );
        }
    })
    .catch(function(err) {
        console.log(err);
        res.status(500).send('Error importing CVS');
    });
});

app.get('/export-custom-collections', (req, res) => {
    const shopify = new Shopify({
        shopName: shopName,
        accessToken: process.env.access_token
    });

    shopify.customCollection.list()
    .then(collections => {
        const fields = ['title', 'sort_order'];
        try {
            const parser = new Json2csvParser(fields);
            const csv = parser.parse(collections);
            fs.writeFile('customCollection.csv', csv, 'utf8', function(err) {
                if (err) {
                    console.log('Error parsing json to csv');
                } else {
                    console.log('File saved');
                }
            });
        } catch (err) {
            console.error(err);
        }
    })
    .catch(err => console.error(err));
});

app.get('/import-custom-collections', (req, res) => {
    const shopify = new Shopify({
        shopName: shopName,
        accessToken: process.env.access_token
    });
    const csvFilePath = 'import.csv';
    csv()
    .fromFile(csvFilePath)
    .then((collectionObject) => {
        for (i in collectionObject) {
            shopify.customCollection.create(collectionObject[i])
            .then(
                customCollection => console.log(customCollection),
                err => console.error(err)
            );
        }
    })
    .catch(function(err) {
        console.log(err);
        res.status(500).send('Error importing CVS');
    });
});

app.get('/import-variant-prices', (req, res) => {
    const shopify = new Shopify({
        shopName: shopName,
        accessToken: process.env.access_token
    });

    const csvFilePath = 'price.csv';
    csv()
    .fromFile(csvFilePath)
    .then((priceObject) => {
        proccessImport(priceObject, shopify);
    })
    .catch(function(err) {
        console.log(err);
        res.status(500).send('Error importing CVS');
    });
});

async function proccessImport(importArray, shopify) {
    for (i in importArray) {
        var productHandle = importArray[i].handle;
        await shopify.product.list({handle: productHandle})
        .then(product => {
            var productVariant = product[0].variants.find(x => x.sku === importArray[i].sku);
            shopify.productVariant.update(productVariant.id, {price: importArray[i].price, compare_at_price: importArray[i].compare_at_price})
            .then(variant => console.log(variant))
            .catch(err => console.error(err));
        })
        .catch(err => console.error(err));
   }
}